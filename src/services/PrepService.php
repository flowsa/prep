<?php
/**
 * Prep plugin for Craft CMS 3.x
 *
 * Prepare json files for import with feedme
 *
 * @link      www.flows.acom
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\prep\services;

use flowsa\prep\Prep;

use Craft;
use craft\base\Component;

/**
 * @author    Richard Frank
 * @package   Prep
 * @since     1.0.0
 */
class PrepService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
