<?php
/**
 * Prep plugin for Craft CMS 3.x
 *
 * Prepare json files for import with feedme
 *
 * @link      www.flows.acom
 * @copyright Copyright (c) 2018 Richard Frank
 */

/**
 * @author    Richard Frank
 * @package   Prep
 * @since     1.0.0
 */
return [
    'Prep plugin loaded' => 'Prep plugin loaded',
];
