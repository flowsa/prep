<?php
/**
 * Prep plugin for Craft CMS 3.x
 *
 * Prepare json files for import with feedme
 *
 * @link      www.flows.acom
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\prep\console\controllers;

use flowsa\prep\Prep;

use Craft;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Default Command
 *
 * @author    Richard Frank
 * @package   Prep
 * @since     1.0.0
 */
class DefaultController extends Controller
{
    // Public Methods
    // =========================================================================

    /**
     * Handle prep/default console commands
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'something';

        echo "Welcome to the console DefaultController actionIndex() method\n";

        return $result;
    }

    /**
     * Handle prep/default/do-something console commands
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'something';

        echo "Welcome to the console DefaultController actionDoSomething() method\n";

        return $result;
    }
}
