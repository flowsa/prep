<?php
/**
 * Prep plugin for Craft CMS 3.x
 *
 * Prepare json files for import with feedme
 *
 * @link      www.flows.acom
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\prep;

use flowsa\prep\services\PrepService as PrepServiceService;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\console\Application as ConsoleApplication;

use yii\base\Event;

/**
 * Class Prep
 *
 * @author    Richard Frank
 * @package   Prep
 * @since     1.0.0
 *
 * @property  PrepServiceService $prepService
 */
class Prep extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Prep
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'flowsa\prep\console\controllers';
        }

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'prep',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
